<?php

declare(strict_types=1);
// ukazka s typehintingem, do metody setName lze nastavit pouze string
// pouziti strict

namespace app;

require('./php.z.typehint.from.3.php');

$test = new Test();

$test->setName(1); // Fatal error: Uncaught TypeError: Argument 1 passed to app\Test::setName() must be of the type string, int given
// $test->setName('jmeno');

var_dump($test); 