<?php
// ukazka s typehintingem, do metody setName lze nastavit pouze string
// zadane cislo prekonvertuje na retezec string(1) "1"

namespace app;

require('./php.z.typehint.from.2.php');

$test = new Test();

$test->setName(1);
// $test->setName('jmeno');

var_dump($test); // string(1) "1"