<?php

// declare(strict_types=1);
// tady deklarace strict_types neresi nic, musi se zadavat v souboru, kde je metoda definovana

// ukazka s return value, do metody getName
// pouziti strict

namespace app;

require('./php.z.returnvalue.from.1.php');

$test = new Test();

$test->getName();
// Fatal error: Uncaught TypeError: Return value of app\Test::getName() must be of the type string, int returned in 
