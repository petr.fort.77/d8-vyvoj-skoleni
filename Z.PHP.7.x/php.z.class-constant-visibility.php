<?php

/*
Priklad na viditelnost u konstat trid a tedy i interface

Na konci je priklad s Reflexi (ta je zpracovana samostatne), zde je zminena pouze v souvislosti s konstantami
*/

/**
 * Testovaci trida na viditelnost konstant
 */
class Token
{
    // Defaultni viditelnost je public
    const PUBLIC_CONST = 0;

    // Konstanty mohou mit take viditelnost
    private const PRIVATE_CONST = 0;
    protected const PROTECTED_CONST = 0;
    public const PUBLIC_CONST_TWO = 0;

    // Deklarace viditelnosti lze zkratit na jeden radek, pak se tyka vsech 
    private const FOO = 1, BAR = 2;
}

// Rozhrani mohou mit pouze public (coz je default) viditelnost
interface ICache
{
    public const PUBLIC = 0;
    const IMPLICIT_PUBLIC = 1;
}

echo sprintf('Token::PUBLIC_CONST: <strong>%s</strong> <br />', Token::PUBLIC_CONST);
print "<br />";
// print Token::PRIVATE_CONST; // Fatal error: Uncaught Error: Cannot access private const Token::PRIVATE_CONST 
print "<br />";
// print Token::PROTECTED_CONST; // Fatal error: Uncaught Error: Cannot access protected const Token::PROTECTED_CONST 
print "<br />";
// print Token::FOO; // Fatal error: Uncaught Error: Cannot access private const Token::BAR
print "<br />";
// print Token::BAR; // Fatal error: Uncaught Error: Cannot access private const Token::BAR
print "<br />";



// Take byla vylepsena Reflexe, aby umoznila nacitat vic, nez jen hodnoty konstant 
class testClass
{
    const TEST_CONST = 'test';
    protected const TEST_PROTECTED_CONST = 'proteced_test';
    private const TEST_PRIVATE_CONST = 'private_test';
}


// Priklad reflexe 
$obj = new ReflectionClass("testClass");
$const = $obj->getReflectionConstant("TEST_CONST");
$consts = $obj->getReflectionConstants();

print '<pre>';
print $obj;
print "<hr />";
print $const;
print "<hr />";
print_r($consts);
print '</pre>';

// Vystup z reflexe - obsahuje lokalni cestu, ta se bude lisit
/*
Class [  class testClass ] {
  @@ /Applications/MAMP/htdocs/projects/Drupal.8.training/Z.PHP.7.x/php.z.class.constant.visibility.php 47-52

  - Constants [3] {
    Constant [ public string TEST_CONST ] { test }
    Constant [ protected string TEST_PROTECTED_CONST ] { proteced_test }
    Constant [ private string TEST_PRIVATE_CONST ] { private_test }
  }

  - Static properties [0] {
  }

  - Static methods [0] {
  }

  - Properties [0] {
  }

  - Methods [0] {
  }
}
Constant [ public string TEST_CONST ] { test }
Array
(
    [0] => ReflectionClassConstant Object
        (
            [name] => TEST_CONST
            [class] => testClass
        )

    [1] => ReflectionClassConstant Object
        (
            [name] => TEST_PROTECTED_CONST
            [class] => testClass
        )

    [2] => ReflectionClassConstant Object
        (
            [name] => TEST_PRIVATE_CONST
            [class] => testClass
        )

)
*/