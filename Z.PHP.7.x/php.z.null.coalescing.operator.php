<?php

// priklad ?? null coalescing operator:

/*
// as an if statement:
if (isset($x)) {
    return $x;
} else {
    return $y;
}

// or, as ternary operator:
return (isset($x) ? $x : $y);

// same statement as the null coalescing operator:
return $x ?? $y;

*/

// if else
if (isset($_GET['term'])) {
    $search_term = $_GET['term']; // vraci hodnotu s $_GET['term'] nebo ''
} else {
    $search_term = ''; // default
}

// Alternative the above code can be written using the ternary operator as
$search_term = isset($_GET['term']) ? $_GET['term'] : ''; // vraci hodnotu s $_GET['term'] nebo ''

// The above code can be rewritten using the null coalescing operator as
$search_term = $_GET['term'] ?? ''; // vraci spravne hodnotu s $_GET['term'] nebo ''

echo $search_term;
