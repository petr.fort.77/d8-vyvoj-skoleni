<?php
/*
Ukazka prace s poli s pouzitim syntaxe "hranatych zavorek" [] jak pro konstrukci, tak i pro destructuring (dekonstrukci?)
Tedy obdobne, jako je to s pouzitim funkce list().
*/

// Vytvoreni pole, kdy 1. index je 0
$array = [1, 2, 3];

// Vytvoreni pole, kdy jsou, jednotlive hodnoty, prirazeny pod klice (indexy)
$array2 = ["a" => 10, "b" => 20, "c" => 30];



// Assigns to $a, $b and $c the values of their respective array elements in $array with keys numbered from zero
// Prirazeni hodnot z pole do promennych postupne podle poradi
[$a, $b, $c] = $array;

// prohozenim poradi sem, prirozene, priradi i jine hodnoty
// [$c, $b, $a] = $array; // $a = 3; 

echo sprintf('print ($a): <strong>%s</strong> <br />', $a);



// Prirazeni hodnot z pole do promennych postupne podle indexu
["a" => $a, "b" => $b, "c" => $c] = $array2;

// prohozeni index
// ["c" => $a, "b" => $b, "a" => $c] = $array2; // $a = 30;

echo sprintf('print ($a): <strong>%s</strong> <br />', $a);


// Specialni priklady pouziti - co lze a co ne

// Toto nelze
// list([$a, $b], [$c, $d]) = [[1, 2], [3, 4]]; // Fatal error: Cannot mix [] and list() 

// Toto take nelze
// [list($a, $b), list($c, $d)] = [[1, 2], [3, 4]]; // Fatal error: Cannot mix [] and list() 

// Ale toto lze
[[$a, $b], [$c, $d]] = [[100, 200], [300, 400]];


echo sprintf('print ($a): <strong>%s</strong> <br />', $a);

// Vystup
/*
print ($a): 1
print ($a): 10
print ($a): 100
*/