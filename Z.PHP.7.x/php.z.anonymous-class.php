<?php

/*
Anonymni tridy
https://wiki.php.net/rfc/anonymous_classes

Anonymni tridy se muzou pouzit, na misto pojmenovanych trid, v pripade ze:

- trida nemusi byt zdokumentovana
- pokud je trida pouziva pouze jednou 

*/


var_dump(new class ($i = 0)
{
    public function __construct($i)
    {
        $this->i = $i;
    }
});

// object(class@anonymous)#1 (1) { ["i"]=> int(0) }

/* implementing an anonymous console object from your framework maybe */
class ConsoleProgram {
    public function bootstrap() {
        // ukazka pouziti sprintf s internima (zacinaji a konci na __) konstantama
        echo "<br />" . sprintf('trida: %s, metoda: %s', __CLASS__, __METHOD__);
    }
}

(new class extends ConsoleProgram
{
    public function main()
    {
        /* ... */ 
    }
})->bootstrap();

