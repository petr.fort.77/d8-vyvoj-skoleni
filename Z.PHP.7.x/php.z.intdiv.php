<?php

/*
https://wiki.php.net/rfc/intdiv

Deleni celych cisel v PHP 7, nove s pouzitim intdiv a s ukazkou zpracovani vyjimek

U prikladu se pouzivaji interni constanty (PHP_INT_MAX a PHP_INT_MIN), ktere definuje obor hodnot celych cisel.
PHP_INT_MAX: 9223372036854775807
PHP_INT_MIN: -9223372036854775808

Je dulezite si vsimnout, ze minimalni cislo je o 1 "vetsi", tzn. pokud toto cislo (-9223372036854775808) vydelim -1, tak mi 
    by me mela vyjit kladna hodnota, tedy: 9223372036854775808, ale to uz je mimo rozsah celych cisel, protoze PHP_INT_MAX je prave o tu
    jednu mensi, tedy "pouze" 9223372036854775807 a proto priklad "PHP_INT_MIN by -1 is not an integer", tedy intdiv(PHP_INT_MIN, -1); 
    vyhodi chybu, ze vysledne cislo jiz neni integer.
Velikost integeru je ovsem zavisla na platforme (32/64)
PHP take nepodporuje unsigned (tedy bezznaminkove) typy
Vice povidani o celych cislech je napriklad zde: https://www.php.net/manual/en/language.types.integer.php

Priklad take ukazuje, velice jednoduche, zachyceni vyjimky/chyby (Exception/Error)

prvnich 6 prikladu je jasnych
zastavime se u poslednich dvou, ktere pri chybe, aritmeticke operace deleni celych cisel, 'vyhazuji' vyjimky, tedy napriklad, ze dane cislo, po deleni:
    - neni platne cele cislo definove v oborem hodnot: PHP_INT_MIN az PHP_INT_MAX: "Fatal error: Uncaught ArithmeticError: Division of PHP_INT_MIN by -1 is not an integer"
    - provadim deleni 0: Fatal error: Uncaught DivisionByZeroError: Division by zero
    
Uplne nejzkladnejsi zachyceni chyby (Error), nebo Vyjimky (Exception) pomoci bloku
try {
    // kod ktery "kontrolujeme/testujeme" na moznou chybu/vyjimky
} catch (Error $e) {
    // zpracovani chyby
} 

Zpracovani vyjimek vyresime samostatne
*/

echo sprintf('PHP_INT_MAX: %s<br />PHP_INT_MIN: %s <br />', PHP_INT_MAX, PHP_INT_MIN);

echo sprintf('var_dump(intdiv(3, 2)): <strong>%s</strong> <br />', intdiv(3, 2));
echo sprintf('var_dump(intdiv(-3, 2)): <strong>%s</strong> <br />', intdiv(-3, 2));
echo sprintf('var_dump(intdiv(3, -2)): <strong>%s</strong> <br />', intdiv(3, -2));
echo sprintf('var_dump(intdiv(-3, -2)): <strong>%s</strong> <br />', intdiv(-3, -2));
echo sprintf('var_dump(intdiv(PHP_INT_MAX, PHP_INT_MAX)): <strong>%s</strong> <br />', intdiv(PHP_INT_MAX, PHP_INT_MAX));
echo sprintf('var_dump(intdiv(PHP_INT_MIN, PHP_INT_MIN)): <strong>%s</strong> <br />', intdiv(PHP_INT_MIN, PHP_INT_MIN));
 

// priklad "PHP_INT_MIN by -1 is not an integer"

// Fatal error: Uncaught ArithmeticError: Division of PHP_INT_MIN by -1 is not an integer
// var_dump(intdiv(PHP_INT_MIN, -1));
echo "<br />";

// zachyceni chyby
try {
    intdiv(PHP_INT_MIN, -1); 
} catch (Error $e) { // ArithmeticError $e
    print $e->getMessage();
}



// priklad "Division by zero"
// Fatal error: Uncaught DivisionByZeroError: Division by zero
// var_dump(intdiv(1, 0));
echo "<br />";

// zachyceni chyby
try {
    var_dump(intdiv(1, 0));
} catch (ArithmeticError $e) { // Error $e | ArithmeticError $e
    print $e->getMessage();
} 

?>