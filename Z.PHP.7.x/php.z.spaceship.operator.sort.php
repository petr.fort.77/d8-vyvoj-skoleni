<?php

	$beverages = array(
		array('beverage' => 'Coke', 'price' => 5),
		array('beverage' => 'Water', 'price' => 2),
		array('beverage' => 'Vodka', 'price' => 21)
	);

	echo "Beverages before sorting <br>";

	print_r($beverages);
	// Array ( [0] => Array ( [beverage] => Coke [price] => 5 ) [1] => Array ( [beverage] => Water [price] => 2 ) [2] => Array ( [beverage] => Vodka [price] => 21 ) )

	echo "<br>";

	usort($beverages,function ($a, $b) {
	    return ($a['price'] < $b['price']) ? -1 : (($a['price'] > $b['price']) ? 1 : 0);
	});

	echo "Beverages after sorting <br>";

	print_r($beverages);
	// Array ( [0] => Array ( [beverage] => Water [price] => 2 ) [1] => Array ( [beverage] => Coke [price] => 5 ) [2] => Array ( [beverage] => Vodka [price] => 21 ) )	
	echo "<br>";

	// nahrazeni obsahu usort pomoci spaceship operatoru
	usort($beverages,function ($a, $b) {
		// return ($a['price'] < $b['price']) ? -1 : (($a['price'] > $b['price']) ? 1 : 0);
		return $a['price'] <=> $b['price'];
	});

	print_r($beverages);
	// Array ( [0] => Array ( [beverage] => Water [price] => 2 ) [1] => Array ( [beverage] => Coke [price] => 5 ) [2] => Array ( [beverage] => Vodka [price] => 21 ) )