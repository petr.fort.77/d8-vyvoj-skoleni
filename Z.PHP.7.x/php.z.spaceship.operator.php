<?php

    //integer comparison
    print( 1 <=> 1);print("<br/>"); // 0
    print( 1 <=> 2);print("<br/>"); // -1
    print( 2 <=> 1);print("<br/>"); // 1
    print("<br/>");
    //float comparison
    print( 1.5 <=> 1.5);print("<br/>"); // 0
    print( 1.5 <=> 2.5);print("<br/>"); // -1
    print( 2.5 <=> 1.5);print("<br/>");
    print("<br/>");
    //string comparison
    print( "a" <=> "a");print("<br/>"); // 0
    print( "a" <=> "b");print("<br/>"); // -1
    print( "b" <=> "a");print("<br/>"); // 1
