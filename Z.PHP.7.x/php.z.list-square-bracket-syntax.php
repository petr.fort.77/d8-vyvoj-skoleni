<?php

// priklad pouziti "Square bracket" syntaxe pro list()
// v ramci listu, lze urcit index na ktery se ma mapovat. Hodi se to v pripade, ze mame pole, ktere nezacina index 0 (nebo ho nema)

$powersOfTwo = [1 => 2, 2 => 4, 3 => 8]; // pole nema index 0

list(1 => $oneBit, 2 => $twoBit, 3 => $threeBit) = $powersOfTwo;
list(3 => $threeBit_2, 2 => $twoBit_2, 1 => $oneBit_2) = $powersOfTwo;
// list($threeBit_2, $twoBit_2, $oneBit_2) = $powersOfTwo; // Notice: Undefined offset: 0


echo sprintf('print_r($oneBit): <strong>%s</strong> <br />', print_r($oneBit, true));
echo sprintf('print_r($twoBit): <strong>%s</strong> <br />', print_r($twoBit, true));
echo sprintf('print_r($threeBit): <strong>%s</strong> <br />', print_r($threeBit, true));
echo '<hr />';
echo sprintf('print_r($threeBit_2): <strong>%s</strong> <br />', print_r($threeBit_2, true));
echo sprintf('print_r($twoBit_2): <strong>%s</strong> <br />', print_r($twoBit_2, true));
echo sprintf('print_r($oneBit_2): <strong>%s</strong> <br />', print_r($oneBit_2, true));

