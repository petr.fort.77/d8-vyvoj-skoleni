<?php

// elvis operator ?:

/*
    // using if/else
    if (expr1) {
        return expr1;
    } else {
        return expr2;
    }

    // using the ternary operator
    expr1 ? expr1 : expr2;

    // using the elvis operator
    expr1 ?: expr2;
*/

// using if/else
if (isset($_GET['term'])) {
    $search_term = $_GET['term']; // pro ?term=ahoj = "ahoj", obecne zadana hodnota
} else {
    $search_term = '';
}

// using the ternary operator
$search_term = (isset($_GET['term']) ? $_GET['term'] : ''); // pro ?term=ahoj = "ahoj", obecne zadana hodnota

// using the elvis operator - POZOR, nelze pouzit pro ziskani hodnoty, vraci vysledek isset ne hodnotu
$search_term = (isset($_GET['term']) ?: '');  // pro ?term=ahoj = 1, pokud je zadana hodnota (staci pouze ?term=) pak true (1), jinak false (0)

echo $search_term;