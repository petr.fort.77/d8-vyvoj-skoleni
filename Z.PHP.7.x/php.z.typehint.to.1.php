<?php
// ukazka bez typehintingu, do metody setName lze nastavit cokoliv

namespace app;

require('./php.z.typehint.from.1.php');

$test = new Test();

$test->setName(1);
// $test->setName('jmeno');

var_dump($test); // int(1) 