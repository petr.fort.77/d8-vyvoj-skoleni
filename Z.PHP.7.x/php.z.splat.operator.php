<?php

// v php od verze 5.6, ale neni prilis znama
// ... alias Splat Operator
// musi byt posledni parametr, za nim uz nesmi byt zadne dalsi
function concate($transform, ...$strings) {
    
    $string = '';

    foreach($strings as $piece) {
        $string .= ' ' . $piece;
    }

    return $transform($string);

}

echo concate('strtoupper', 'ahoj', 'jak', 'se', 'mate');
// AHOJ JAK SE MATE
