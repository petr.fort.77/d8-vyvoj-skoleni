<?php

/**
 * Testovaci trida pro Iterator
 */
class TestIterator implements Iterator {

    private $data = [];
    private $index = 0;

    /**
     * konstruktor s ukazkou typehintingu 
     *
     * @param array $data pole
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    // interface Iterator

    /**
     * {@inheritdoc}
     */
    public function current()
    {
        return $this->data[$this->index];
    }

    /**
     * {@inheritdoc}
     */
    public function next() {
        $this->index++;
    }

    /**
     * {@inheritdoc}
     */
    public function key() {
        return $this->index;
    }

    /**
     * {@inheritdoc}
     */
    public function valid() {
        return isset($this->data[$this->key()]);
        // return isset($this->data[$this->index);
    }

    /**
     * {@inheritdoc}
     */
    public function rewind() {
        $this->index = 0;
    }

    // Custom methods

    /**
     * Vlastni metoda jiz mimo interface Iterator, ktera obraci pole
     *
     * @return void
     */
    public function reverse() {
        $this->data = array_reverse($this->data);
        $this->rewind();
    }

    /**
     * Priklad "magicke" metody, ktera umoznuje vypis (echo|print|..) v ramci instance
     *
     * @return string
     */
    public function __toString() {
        
        // return print_r($this->data, true);

        $o = '';

        // pro vypis se muze pouzit foreach, protoze je implementovan interface Iterator
        foreach ($this->data as $key => $value) {
            $o .= $key . ' - ' . $value;
            $o .= "<br />";
        }

        return $o;
    }

}

$test = new TestIterator(['Řeznický učeň', 'Rvačka', 'Och, doktore!', 'Fatty vesnickým hrdinou', 'Fatty se žení', 'Fatty na pláži', 'Lechtivý desperádo', 'Hotelový poslíček', 'Fatty v sanatoriu']);

print $test; // pouzije se magicka metoda __toString

echo "<hr />";

// print_r($test->data); // Fatal error: Uncaught Error: Cannot access private property TestIterator::$data

// pruchod pomoci Iteratoru. A i presto, ze $data jsou privatni - nelze k nim pristoupit primo, napriklad: print_r($test->data), 
// tak pomoci public metod (z interface Iterator) se k nim pristupovat dat, zde ve foreach, i pres $key a $value
foreach ($test as $key => $value) {
    echo $key . ' - ' . $value; 
    echo "<br />";
}

// Vystup:
/*
0 - Řeznický učeň
1 - Rvačka
2 - Och, doktore!
3 - Fatty vesnickým hrdinou
4 - Fatty se žení
5 - Fatty na pláži
6 - Lechtivý desperádo
7 - Hotelový poslíček
8 - Fatty v sanatoriu
------------------------------------------------------------------
0 - Řeznický učeň
1 - Rvačka
2 - Och, doktore!
3 - Fatty vesnickým hrdinou
4 - Fatty se žení
5 - Fatty na pláži
6 - Lechtivý desperádo
7 - Hotelový poslíček
8 - Fatty v sanatoriu
*/
