<?php

/**
 * Testovaci trida pro ArrayAccess
 */
class TestIterator implements ArrayAccess {

    private $data = [];

    /**
     * konstruktor s ukazkou typehintingu 
     *
     * @param array $data pole
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    // interface ArrayAccess
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->data);
    }

    public function offsetGet($offset)
    {
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value)
    {
        // doplneni logiky v pripade, ze udelam napriklad $test[] = 'ahoj'
        // bez nasledujiciho kodu, byl se pridala data bez indexu, respektive s prazdnym indexem, ktery se prepisoval
        // i pro dalsi pridani, napriklad: $test[] = 'Bláznivá domácnost'; $test[] = 'Zákulisí';  by se nastavil jen posledni 'Zákulisí'
        if (empty($offset)) {
            $offset = count($this->data);
        }

        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }    

  
    // Custom methods

    /**
     * Vlastni metoda jiz mimo interface Iterator, ktera obraci pole
     *
     * @return void
     */
    public function reverse() {
        $this->data = array_reverse($this->data);
        $this->rewind();
    }

    /**
     * Priklad "magicke" metody, ktera umoznuje vypis (echo|print|..) v ramci instance
     *
     * @return string
     */
    public function __toString() {
        
        // return print_r($this->data, true);

        $o = '';

        // pro vypis se muze pouzit foreach, protoze je implementovan interface Iterator
        foreach ($this->data as $key => $value) {
            $o .= $key . ' - ' . $value;
            $o .= "<br />";
        }

        return $o;
    }

}

$test = new TestIterator(['Řeznický učeň', 'Rvačka', 'Och, doktore!', 'Fatty vesnickým hrdinou', 'Fatty se žení', 'Fatty na pláži', 'Lechtivý desperádo', 'Hotelový poslíček', 'Fatty v sanatoriu']);

print $test; // pouzije se magicka metoda __toString

echo "<hr />";

// pokud neni implementovano rozhrani 'ArrayAccess'
//  Fatal error: Uncaught Error: Cannot use object of type TestIterator as array in
$test[] = 'Bláznivá domácnost';
$test[] = 'Zákulisí';
$test[] = 'Fatty sebevrahem';
$test[] = 'Fatty v garáži';
$test[] = 'Sehnání stáda dobytka';

print $test; // pouzije se magicka metoda __toString

echo "<br />";

echo 'echo $test[2]: ' . $test[2]; // Och, doktore!
echo "<br />";
$test[2] = 'doktore!!!';
echo 'echo $test[2]: ' . $test[2]; // Och, doktore!


