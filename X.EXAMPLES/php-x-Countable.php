<?php

/**
 * Testovaci trida pro Countable
 */
class TestIterator implements Countable {

    private $data = [];

    /**
     * konstruktor s ukazkou typehintingu 
     *
     * @param array $data pole
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    // interface Countable
    public function count() : int
    {
        return count($this->data);
    }



    public function offsetGet($offset)
    {
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value)
    {
        // doplneni logiky v pripade, ze udelam napriklad $test[] = 'ahoj'
        // bez nasledujiciho kodu, byl se pridala data bez indexu, respektive s prazdnym indexem, ktery se prepisoval
        // i pro dalsi pridani, napriklad: $test[] = 'Bláznivá domácnost'; $test[] = 'Zákulisí';  by se nastavil jen posledni 'Zákulisí'
        if (empty($offset)) {
            $offset = count($this->data);
        }

        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }    

  
    // Custom methods

    /**
     * Vlastni metoda jiz mimo interface Iterator, ktera obraci pole
     *
     * @return void
     */
    public function reverse() {
        $this->data = array_reverse($this->data);
        $this->rewind();
    }

    /**
     * Priklad "magicke" metody, ktera umoznuje vypis (echo|print|..) v ramci instance
     *
     * @return string
     */
    public function __toString() {
        
        // return print_r($this->data, true);

        $o = '';

        // pro vypis se muze pouzit foreach, protoze je implementovan interface Iterator
        foreach ($this->data as $key => $value) {
            $o .= $key . ' - ' . $value;
            $o .= "<br />";
        }

        return $o;
    }

}

$test = new TestIterator(['Řeznický učeň', 'Rvačka', 'Och, doktore!', 'Fatty vesnickým hrdinou', 'Fatty se žení', 'Fatty na pláži', 'Lechtivý desperádo', 'Hotelový poslíček', 'Fatty v sanatoriu']);

print $test; // pouzije se magicka metoda __toString

echo "<hr />";

// pouze pokud je implementovani rozhrani Countable
echo 'echo count($test): ' . count($test); // 9
// pokud ne: 
// Warning: count(): Parameter must be an array or an object that implements Countable
