<?php

/**
 * Trida pro praci s priklady
 * definovana jako final - tzn. nelze z ni uz dedit
 */
final class ExampleFiles {

    /**
     * @var string adresar kde jsou soubory
     */
    private $dir = '';

    /**
     * @var array nazvy souboru s priklady
     */
    private $files = [];

    /**
     * konstrukter, lze urcit i jiny adresar
     * @param string $dir adresar
     */
    public function __construct($dir = '.') {
        
        $this->setDir($dir);

    }

    /**
     * inicializace
     *
     * @return void
     */
    public function init() {
        
        $this->files = $this->readFiles();        

    }

    /**
     * Setter pro adresar - pouze z duvodu odseknuti pripadneho koncoveho lomitka
     *
     * @return void
     */
    private function setDir($dir) {
        
        $this->dir = preg_replace('/\/$/', '', $dir);

    }


    /**
     * nacteni seznamu souboru z adresare
     *
     * @return array
     */
    private function readFiles() {
        
        $files = array();
        
        $list = glob($this->dir . "/*.php"); // nacteni php souboru
        
        natsort($list); // jejich "prirozene" serazeni, tedy 1,2,3..,9,10,11 misto 1,10,11,..17,2,3,4

        foreach ($list as $filename) {            
            $files[] = preg_replace('/^.\//', '', $filename); // odseknuti pocatecniho ./ z nazvu souboru
        }

        return $files;

    }

    /**
     * Vypis produktu
     * @return string HTML
     */
    public function __toString() {
        
        $o = '';
        
        if (count($this->files) > 0) {
            $o .= '<ul>';
            foreach ($this->files as $i => $filename) {                
                $o .= "<li><a href=\"{$filename}\">{$filename}</a></li>";
            }
            $o .= '</ul>';
        } else {
            $o .= '<p>Nejsou k dispozici zadne priklady</p>';
        }

        return $o;
    }

}
