<?php

// vlozeni zavislosti pres konstrukter
class Customer
{
    public function __construct($mysqli)
    {
        // ...
    }

    public function loadByEmail($email)
    {
        echo "pripojeni k databazi" . "<br />";
        // ...
        echo "dotaz do databaze pro email ${email}" . "<br />";
    }    
}

// $mysqli = new mysqli('127.0.0.1', 'username', 'password', 'database');
$mysqli = (object) []; // testovaci objekt

$customer = new Customer($mysqli);
$customer->loadByEmail('jmeno.prijmeni@upce.cz');

