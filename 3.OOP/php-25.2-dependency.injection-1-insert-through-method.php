<?php

// vlozeni zavislosti pres parametr metody
class Customer
{
    public function loadByEmail($email, $mysqli)
    {
        echo "pripojeni k databazi" . "<br />";
        // ...
        echo "dotaz do databaze pro email ${email}" . "<br />";
    }
}

// $mysqli = new mysqli('127.0.0.1', 'username', 'password', 'database');
$mysqli = (object)[]; // testovaci objekt

$customer = new Customer();
$customer->loadByEmail('jmeno.prijmeni@upce.cz', $mysqli);


