
<?php

// test setteru

/**
 * Undocumented class
 */
class Test {

    /**
     * Undocumented variable
     *
     * @var integer
     */
    private $a = 0;

    /**
     * Undocumented function
     *
     * @param int $a
     */
    function __construct($a) { // idealne pak i tady: int $a
    
        //    $this->a = $a; // nezkotroluje type   
       $this->setA($a); // resit pres setter
    }

    /**
     * Undocumented function
     *
     * @param integer $a
     * @return void
     */
    public function setA(int $a) {
        $this->a = $a;
    }
     

}

// $test = new Test('aaa'); error
$test = new Test(10);// OK

