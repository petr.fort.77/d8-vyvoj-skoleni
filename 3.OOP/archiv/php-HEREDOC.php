

        return <<<HEREDOC
A fantastic death machine, made to be extra cold and
intimidating. It comes complete with a "super-laser"
capable of destroying planets, with a range of $range.
HEREDOC;