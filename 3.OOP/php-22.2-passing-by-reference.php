<?php
// Citace z knihy: Branko Ajzele. „Mastering PHP 7.“. 

class Util
{
    // predani promenne odkazem, s pouzitim & pred promennou
  function hello(&$msg)
  {
    $msg = "<p>Welcome $msg</p>"; // zde jiz dojde i k uprave globalni promenne $str
    return $msg;
  }
}

$str = 'John'; // globalni promenna $str

$obj = new Util();
echo $obj->hello($str); // Welcome John

echo $str; // Welcome John“
