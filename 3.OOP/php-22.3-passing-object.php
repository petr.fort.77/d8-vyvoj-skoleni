<?php

// Citace z knihy: Branko Ajzele. „Mastering PHP 7.“

class User
{
  public $salary = 4200;
}

function bonus(User $u)
{
  $u->salary = $u->salary + 500;
}

$user = new User();
echo $user->salary; // 4200
bonus($user);
echo "<br />";
echo $user->salary; // 4700 - hodnota se zmenila
