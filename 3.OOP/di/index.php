<?php

require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;


interface ConfigInterface
{
    public function getValue($value);

    public function setValue($path, $value);
}

interface LoggerInterface
{
    public function log($message);
}

class Config implements ConfigInterface
{
    protected $config = [];

    public function getValue($value)
    {
        // implementation
    }

    public function setValue($path, $value)
    {
        // implementation
    }
}

class Logger implements LoggerInterface
{
    public function log($message)
    {
        // implementation
    }
}

class App
{
    protected $config;
    protected $logger;

    public function __construct(config $config, logger $logger) // POZOR, zde doslo k nahrazeni nazvu interface nazvem sluzby
    {
        $this->config = $config;
        $this->logger = $logger;
    }

    public function run()
    {
        $this->config->setValue('executed_at', time());
        $this->logger->log('executed');
    }
}

// Bootstrapping
$container = new ContainerBuilder();

$loader = new YamlFileLoader($container, new FileLocator(__DIR__));
$loader->load('container.yml');
// pozor 

$container->compile();


// Client code
$app = $container->get('app');
$app->run();

var_dump($app);
