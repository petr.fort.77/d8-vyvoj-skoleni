<?php

// Nepresna citace z knihy: Branko Ajzele. „Mastering PHP 7.“ Knihy. 

/**
 * Formatovac
 */
trait Formatter {

    // metoda pro formatovani
    public function formatPrice($price) {
        
        return sprintf('%.2F', $price);

    }

}

class Ups {

    use Formatter; // pouziti stejneho traitu Formatter

    private $price = 4.9999;

    public function getShippingPrice() {

        return $this->formatPrice($this->price);

    }
}


class Dhl {

    use Formatter; // pouziti stejneho traitu Formatter

    private $price = 9.4999;

    public function getShippingPrice()
    {
        return $this->formatPrice($this->price);
    }

}

$ups = new Ups();
echo $ups->getShippingPrice(); // 5.0

echo "<br />";

$dhl = new Dhl();
echo $dhl->getShippingPrice(); // 9.5

// new Formatter();
// Fatal error: Uncaught Error: Cannot instantiate trait Formatter 