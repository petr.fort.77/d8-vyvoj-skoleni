<?php

// ukazka getteru getName a magicke metody __get

class Animal {

    private $name;
    private $nickname;

    public function __construct($name, $nickname)
    {
        $this->name = $name;
        $this->nickname = $nickname;
    }

    /**
     * getter pro name
     *
     * @return string
     */
    public function getName() : string {
        return $this->name;
    }

    /**
     * magicka metoda __get
     *
     * @param string $name
     * @return void
     */
    public function __get(string $name) {
        
        // dalsi logika

        // kontrola na existenci

        // navratova hodnota
        return $this->$name;

    }


}

$cat = new Animal('cat', 'cert');

print $cat->getName();
echo "<br />";
// print $cat->nickname; // Fatal error: Uncaught Error: Cannot access private property Animal::$nickname
// print $cat->test; // Notice: Undefined property: Animal::$test
print $cat->nickname; // cert - pouze diky magicke metode __get