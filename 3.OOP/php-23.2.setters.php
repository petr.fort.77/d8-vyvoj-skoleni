<?php

// ukazka setteru setName a magicke metody __set

class Animal2
{

    private $name;
    private $nickname;

    /**
     * getter pro name
     *
     * @return string
     */
    public function setName(?string $name): void
    {
        // dalsi logika

        $this->name = $name;
    }

    /**
     * getter pro name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * magicka metoda __set
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set(string $name, $value)
    {
        // dalsi logika

        // nastaveni hodnoty
        $this->$name = $value;
    }
}

$cat = new Animal2();

// $cat->setName(null);
$cat->setName('cat');
print $cat->getName();
echo "<br />";
$cat->ahoj = 'cau';
$cat->nickname = 'cert';
var_dump($cat);