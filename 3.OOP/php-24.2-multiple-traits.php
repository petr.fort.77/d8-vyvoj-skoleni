<?php

// Nepresna citace z knihy: Branko Ajzele. „Mastering PHP 7.“ Knihy. 

/**
 * Csv import/exporter
 */
trait CsvHandler {

    public $header = true;
    
    public static $counter = 0;

    public function getCounter() {
        
        return self::$counter;

    }

    // import
    public function import() {
        
        echo 'CsvHandler > import' . "<br />";

    }

    // import
    public function export() {
        
        echo 'CsvHandler > export' . "<br />";

    }

}

/**
 * Xml import/exporter
 */
trait XmlHandler
{

    // import
    public function import()
    {

        echo 'XmlHandler > import' . "<br />";
    }

    // import
    public function export()
    {

        echo 'XmlHandler > export' . "<br />";
    }

    abstract public function testA(); // ukazka abstrakni metody
}


class SalesOrder {

    use CsvHandler, XmlHandler {
        XmlHandler::import insteadof CsvHandler;
        CsvHandler::export insteadof XmlHandler;
        XmlHandler::export as exp;
    }

    // use CsvHandler, XmlHandler; // Fatal error: Trait method import has not been applied, because there are collisions with other trait methods on SalesOrder

    // metodu lze i prepsat, tedy nahradit tu naimportovanou
    // public function import() {
    //     echo "SalesOrder > import" . "<br />";
    // }

    public function initImport() {
        $this->import();
    }

    public function initExport() {
        $this->export();
        $this->exp();
    }

    public function testA() { // implementace abstrakni tridy

    }

}

$order = new SalesOrder();

$order->initImport();
// XmlHandler > import

$order->initExport();
// CsvHandler > export
// XmlHandler > export

echo $order->header;

// ukazka staticke promenne
echo "<br />";
$order::$counter+=5;
echo $order->getCounter();
$order::$counter += 10;
echo "<br />";
echo $order->getCounter();