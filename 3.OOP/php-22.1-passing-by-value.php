<?php
// Citace z knihy: Branko Ajzele. „Mastering PHP 7.“. 

class Util
{
    // defaultni odkaz parametru hodnotou
  function hello($msg)
  {
    $msg = "<p>Welcome $msg</p>"; // tato uprava se neprojevi na globalni promenne $str
    return $msg;
  }
}

$str = 'John'; // globalni promenna $str

$obj = new Util();
echo $obj->hello($str); // Welcome John

echo $str; // John“
