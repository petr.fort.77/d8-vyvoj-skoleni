<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/**
 * Rozhrani je "predpis"/"sablona" pro tridu
 */
interface Dog
{

    /**
     * Rozhrani muze definovat metody, ale ne atributy
     * Metody definove v rozhrani neobsahuji kod, pouze vzory metod, ktere musi trida, ktera rozhrani implementuje, definovat
     */
    public function bark();
}

class Labrador implements Dog
{

}



// Fatal error: Class Labrador contains 1 abstract method and must therefore be declared abstract or implement the remaining methods (Dog::bark) in /Applications/MAMP/htdocs/projects/Drupal.8.training.oop/php-10-animals-interface.php on line 15
$cert = new Labrador();
