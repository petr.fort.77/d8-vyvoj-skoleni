<?php
// Encapsulation - zapouzdreni
// Abstraction - abstrakce, doplneni parametru pro znovupouzitelnost s konfiguraci
function calculation($total = 0) {
    
    $total += 8;
    
    $total -= 2;
    
    $total /= 2;
    
    $total *= 6;
    
    return $total;
}

print calculation(); // 18
print "<br />";
print calculation(4); // 30

