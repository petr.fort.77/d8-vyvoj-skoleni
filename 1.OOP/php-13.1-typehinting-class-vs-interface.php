<?php

// Konkretni priklady vyhody pouziti typehintingu interface oproti class
// Tedy vyzadovani urciteho "typu" (instanceof) parametru
// Priklad s pouzitim class, kdy je 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/**
 * Rozhrani A
 */
interface LogovaciInterface
{
    public function zalogujZpravu($zprava) : bool;
}


/**
 * Zatim se nepouziva
 */
class Alpha {

}



/**
 * prazdna trida A pro dedeni
 */
// class A extends Alpha { 
class A { 

}

/**
 * prazdna trida B pro dedeni
 */
class B extends A {

}

/**
 * prazdna trida C pro dedeni
 */
class C extends B {

}


/**
 * trida Trida1 dedi z C (tedy tim padem i z B a A) a implementuje LogovaciInterface
 */
class Trida1 extends C implements LogovaciInterface
{

    public function zalogujZpravu($zprava) : bool
    {        
        print "class Trida1 extends C implements LogovaciInterface, zprava: {$zprava}";
        if (strlen($zprava) > 10) {
            return true;
        } else {
            return false;
        }
        
    }

}

$trida1 = new Trida1();

// testovani vypis
echo '$trida1 instanceof LogovaciInterface - ' . $trida1 instanceof LogovaciInterface . "<br />"; // true
echo '$trida1 instanceof Trida1 - ' . $trida1 instanceof Trida1 . "<br />"; // true
echo '$trida1 instanceof A - ' . $trida1 instanceof A . "<br />"; // true
echo '$trida1 instanceof B - ' . $trida1 instanceof B . "<br />"; // true
echo '$trida1 instanceof C - ' . $trida1 instanceof C . "<br />"; // true




/**
 * prazdna trida X pro dedeni
 */
// class X extends Alpha {
class X {

}

/**
 * prazdna trida Y pro dedeni
 */
class Y extends X {

}

/**
 * trida Trida2 dedi z Y (tedy tim padem i z X) a implementuje take LogovaciInterface
 */
class Trida2 extends Y implements LogovaciInterface
{

    public function zalogujZpravu($zprava): bool
    {
        print "class Trida2 extends Y implements LogovaciInterface";
        if (strlen($zprava) % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

}

$trida2 = new Trida2();

// testovani vypis
echo '$trida2 instanceof LogovaciInterface - ' . $trida2 instanceof LogovaciInterface . "<br />"; // true
echo '$trida2 instanceof Trida2 - ' . $trida2 instanceof Trida2 . "<br />"; // true
echo '$trida2 instanceof X - ' . $trida2 instanceof X . "<br />"; // true
echo '$trida2 instanceof Y - ' . $trida2 instanceof Y . "<br />"; // true





/**
 * Nekde jinde v systemu...
 * Objekt TestObject ocekava v metode provedOperaci objekt, se kterym neco provede a pak chce, z vyuzitim logiky toho predaneho objektu
 * provest zalogovani (implementace se lisi v ramci kazdeho objektu)
 */
class TestObject
{
    
    /*
     * jako parametr ocekavani objekt a ne rozhrani
     * priklad type hinting, kdy je vyzadovane objekt
     * 
     * @param \Trida2 $objekt;
     */
    function provedOperaci(Trida2 $objekt) // $objekt instanceof Trida2
    // function provedOperaci(Trida1 $objekt) // varianta s Tridou1
    // function provedOperaci(Alpha $objekt) // varianta s Alpha
    // function provedOperaci(LogovaciInterface $objekt) // $objekt instanceof LogovaciInterface
    {
        // ...

        $result = $objekt->zalogujZpravu('test');
        echo "!" . $result . "!" . "<br />";
        
        // ...
    }

    // ..
}

$tObj = new TestObject();

// volani s nevhodnym parametrem - dokonce i IDE upozornuje na chybu
// $tObj->provedOperaci($trida1); // Fatal error: Uncaught TypeError: Argument 1 passed to TestObject::provedOperaci() must be an instance of Trida2, instance of Trida1 given,

// $tObj->provedOperaci($trida1);
$tObj->provedOperaci($trida2);

/*

Abysme mohli pouzit metodu provedOperaci tridy TestObject univerzalne, tedy tak, abysme mohli, do metody provedOperaci, 
predavat oba objekty , tedy jak $trida1, tak i $trida2, pak bysme museli provest, narpiklad, nasleduji upravy:
zdedit Trida1 i Trida2 ze spolecne tridy, ktera budto nema jiny vyznam, nez aby umoznila to, co muzeme resit pomoci interface,
nebo v sobe obsahuje dalsi, nevyzadanou strukturu/logiku, kterou, ve tridach Trida1 a Trida2 nechceme

class A extends Alpha {  
...
class X extends Alpha {
...
function provedOperaci(Alpha $objekt) // varianta s Alpha
... 


nebo muzeme pouzit interface, kdy zmenu provedeme pouze v ramci typu $objektu:

function provedOperaci(LogovaciInterface $objekt) // $objekt instanceof LogovaciInterface


*/