<?php
// zapouzdreni souvisejich funkci do tridy a vytvoreni jeji instance tedy objektu.

// nazev tridy se pise s prvni velkym pismenem (je to konvence, aby se poznala trida (instance je pak malym), podobne jako u js)
class Calculator {

    // atribut (property)
    public $total = 0; // public - tedy verejny atribut, v instanci k nemu muzeme pristupovat, napriklad $calculator->total;
    // k atributum pristupujeme, v ramci tridy pomoci $this->, tedy napriklad $this->total
    // dalsi moznosti viditelnosti (tedy "public", jsou "protected" a "private" - ty nejsou z "venku" pristupne)
    // platnost (scope) je pouze v ramci dane tridy (objektu)

    // pokud nema metoda explicitne danou viditelnost, pouzije se public, plati zde stejna pravidla, jako u atributu, tzn. public metodu muzu volat z instance
    // pokud bych chtel pristupovat/volat, jinou metodu objektu, muzu pouzit zase $this->, napriklad $this->multiply(3);
    function add($a) {
        $this->total += $a;
    }
    
    function substract($a) {
        $this->total -= $a;
    }
    
    function divide($a) {
        $this->total /= $a;
    }
    
    function multiply($a) {
        $this->total *= $a;
    }
    
}

/**
 * Rozsireni jiz existujici tridy pomoci klicoveho slova extends
 * ve tridy zustanou vsechny predchozi metody a atributy
 * ale ty metody a atributy, ktere byli definovany jako protected, budou nyni private
 */
class WholeNumberCalculator extends Calculator {

    /**
     * Je dobry zvykem zapisovat i viditelnost (public, atd..)
     */
    public function divide($a) {

        parent::divide($a); // pomoci konstrukce parent:: zavolame metodu z nadrazene (z te z ktere dedime) tridy
        
        $this->total = round($this->total); // zaokrouhlime hodnotu po deleni

    } 

    
    /**
     * Nova metova, ktera kontroluje, jestli je hodnota licha
     */
    public function isOdd() {
        
        return ($this->total % 2) === 1;

    }

}

$calculator = new Calculator(); // vytvoreni instance tridy, tedy objektu, pomoci konstruktu new
$wholeNumberCalculator = new WholeNumberCalculator(); // celociselna kalkulacka

$calculator->add(8); // volani public metody
$calculator->substract(2);
$calculator->divide(2.4); // (6 / 2.4 = 2.5)
$calculator->multiply(6);

print $calculator->total; // 15

print '<br />';

$wholeNumberCalculator->add(8);
$wholeNumberCalculator->substract(2);
$wholeNumberCalculator->divide(2.4); // (6 / 2.4 = 2.5 => zaokrouhleno 3)
$wholeNumberCalculator->multiply(6);

print $wholeNumberCalculator->total; // 18

print '<br />';

print !$wholeNumberCalculator->isOdd(); // 1 - pozor, 18 je sude cislo a toto je kontrola na liche (tedy ! na zacatku)
