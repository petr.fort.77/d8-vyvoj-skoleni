<?php

/**
 * Interface CalculatorInterface
 */
interface CalculatorInterface {

    /**
     * Spocitane cislo
     * 
     * @return int|float
     */
    public function getTotal();

    /**
     * Nastaveni vysledne hodnoty
     * 
     * @param int $a libovolne cislo
     */
    public function setTotal($a);

    /**
     * Pricte cislo k vysledne hodnote
     * 
     * @param int $a libovolne cislo
     */
    public function add($a);
    // ... 

}

/**
 * Priklad tridy, kde je videt nacteni dokumentace z interface pomoci {@inheritdoc}
 */
class Calculator implements CalculatorInterface {

    /**
     * {@inheritdoc}
     */
    public function getTotal()
    {
        
    }

}
