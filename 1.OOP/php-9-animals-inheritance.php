<?php

class Animal {
    public $legs = 0;
    public $noise = '';
    
    public function vocalize() {
        print $this->noise . " !!!" . '<br />';
    }

}

class Labrador extends Animal {
    public $legs = 4;
    public $noise = 'haf haf';
}

class Parrot extends Animal {
    public $legs = 1;
    public $noise = 'mnau';

    public function vocalize($words = '') {
        print "{$words} {$this->noise}" . "<br />";
    }

    // toto v php nelze - tedy overload
    // public function vocalize($words = '', $test = '') {
    //     print "{$words} {$this->noise}" . "<br />";
    // }


}

$lump = new Labrador();
$lump->vocalize();
print $lump->legs;

print '<br />';
$papouch = new Parrot();
$papouch->vocalize();
$papouch->vocalize('ahoj');
print $papouch->legs;

// vystup:
// haf haf !!!
// 4
// mnau
// ahoj mnau
// 1
