<?php

class Animal {

    public $legs = 0;
    public $noise = '';

    public function vocalize() {
        print $this->noise . "!";
    }

}

interface Dog {
    public function bark();
}

class Labrador extends Animal implements Dog {

    public $legs = 4;
    public $noise = 'haf';
    
    public function vocalize() {
        $this->bark();
    }
    
    public function bark() {
        for ($i = 0; $i < 3; $i ++) {
            print $this->noise . "!";
        }
    }

}

$lump = new Labrador();
$lump->vocalize();

