<?php


/**
 * Abstraktni trida
 * nemuze byt final - tedy aby nesla zdedit
 * a musi byt definovana jako abstract (abstract class), protoze obsahuje minimalne jednu abstraktni metodu
 */
abstract class Base 
{
    // abstrakni trida, ale pozor i interface, mohou obsahovat konstanty, u tech lze, u trid, definovat viditelnost
    const BASE_ALIAS = 'root';
    protected const MY_PROTECTED_CONST = 'moje protected konstana';

    // abstraktni trida muze obsahovat atributy (properties)
    public $name = 'base class property name'; // viditelnost muze byt: private, protected, public

    // toto je abstraktni metoda - nemuze byt, logicky, private (default public), zbytek ano
    abstract function printdata();

    // toto neni abstraktni metoda - dedi se i s definici
    public function pr()
    {
        echo "Base class" . "<br />";
    }
}

/**
 * trida zdedena z abstraktni tridy - musi definovat vsechny abstraktni tridy
 */
final class Derived extends Base
{
    function printdata()
    {
        echo "Derived class" . "<br />";
    }
}


// odkomentovanim tohoto radku dojde k chybe, protoze nelze vytvaret instance z abstraktnich trid
// $b = new Base();  

$b1 = new Derived;
// echo $b1::MY_PROTECTED_CONST . "<br />"; // ke konstane MY_PROTECTED_CONST se pristupuje "staticky", tedy pres "::", ale protoze je protected, tak se k ni nedostanu
echo $b1::BASE_ALIAS . "<br />"; // ke konstane BASE_ALIAS se pristupuje "staticky", tedy pres "::"
$b1->printdata(); 
$b1->pr(); 
echo $b1->name;

?>