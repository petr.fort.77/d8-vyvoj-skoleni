<?php

// Ukazka dedeni v nastavenim viditelnost public, protected, private

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/**
 * trida A
 */
class A {

    /**
     * atribut a
     *
     * @var string
     */
    private $a = "private";
    
    /**
     * atribut b
     *
     * @var string
     */
    protected $b = "protected";

    /**
     * atribut c
     *
     * @var string
     */
    public $c = "public";

    /**
     * zobrazeni atributu
     *
     * @return void
     */
    public function show()
    {
        echo 'class A - ' . $this->a . "<br />"; // OK - private
        echo 'class A - ' . $this->b . "<br />"; // OK - hlasi se jako protected
        echo 'class A - ' . $this->c . "<br />"; // OK
    }

}

/**
 * trida B (dedi z A)
 */
class B extends A {

    /**
     * zobrazeni atributu
     *
     * @return void
     */
    public function show() {
        // echo $this->a; // Notice: Undefined property: B::$a
        echo 'class B extends A - ' . $this->b . "<br />"; // OK - hlasi se jako protected
        echo 'class B extends A - ' . $this->c . "<br />"; // OK
    }

}

/**
 * trida C (dedi z B)
 */
class C extends B {

    public function show()
    {
        // echo $this->a; // Notice: Undefined property: B::$a
        echo 'class C extends B - ' . $this->b . "<br />"; // OK - hlasi se jako protected
        echo 'class C extends B - ' . $this->c . "<br />"; // OK
    }

}

/*
    Zkuste si odkomentovat jednotlive radky ($objA->a, atd..) 
*/

$objA = new A();
$objA->show();
// $objA->a; // Fatal error: Uncaught Error: Cannot access private property A::$a 
// $objA->b; // Fatal error: Uncaught Error: Cannot access protected property A::$b
// $objA->c; // OK

$objB = new B();
$objB->show();
// $objB->a; // Notice: Undefined property: B::$a 
// $objB->b; // Fatal error: Uncaught Error: Cannot access protected property B::$b
// $objB->c; // OK

$objC = new C();
$objC->show();
// $objC->a; // Notice: Undefined property: C::$a in
// $objC->b; // Fatal error: Uncaught Error: Cannot access protected property C::$b
// $objC->c; // OK

print '$objA instanceof A - ' . $objA instanceof A . '<br />'; // true 
print '$objB instanceof A - ' . $objB instanceof A . '<br />'; // true, dedi z A
print '$objC instanceof A - ' . $objC instanceof A . '<br />'; // true, prestoze dedi pouze z B (ale B dedi a A)

print '$objB instanceof A - ' . $objB instanceof B . '<br />'; // true 
print '$objC instanceof B - ' . $objC instanceof B . '<br />'; // true, dedi z B

/*
Zajimavy je i "pokus" se zakomentovanim cele metody show v ramci jednotlivych, zdedenych trid (B a C) 
*/