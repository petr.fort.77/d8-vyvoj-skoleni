<?php

/**
 * Rozhrani striktne popisuje specifikaci metod
 */
interface DoesStuff {
    public function doStuff();
}

class SomeThing implements DoesStuff {
    public function doStuff()
    {
        print 'parada!';
    }
}

// ...

/**
 * Nekde jinde v systemu...
 * jako parametr ocekavani rozhrani a ne tridu
 * priklad type hinting, kdy je vyzadovane rozhrani
 * 
 * @param \DoesStuff $specialThing;
 */
function UseAnObject(DoesStuff $specialThing) {
    $specialThing->doStuff();
    // ...
}

