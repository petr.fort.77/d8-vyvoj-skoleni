<!doctype html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>1.OOP</title>
</head>

<?php

// zatim bez autoloadu a namespace
require_once('../utils/src/ExampleFiles.php');

?>

<body>
    
    <?php
        $exampleFiles = new ExampleFiles();
        $exampleFiles->init();
        print $exampleFiles;
    ?>

</body>

</html>
