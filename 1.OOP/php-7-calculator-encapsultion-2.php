<?php
// zapouzdreni souvisejich funkci do tridy a vytvoreni jeji instance tedy objektu.

// nazev tridy se pise s prvni velkym pismenem (je to konvence, aby se poznala trida (instance je pak malym), podobne jako u js)
class Calculator {

    // atribut (property)
    public $total = 0; // public - tedy verejny atribut, v instanci k nemu muzeme pristupovat, napriklad $calculator->total;
    // k atributum pristupujeme, v ramci tridy pomoci $this->, tedy napriklad $this->total
    // dalsi moznosti viditelnosti (tedy "public", jsou "protected" a "private" - ty nejsou z "venku" pristupne)
    // platnost (scope) je pouze v ramci dane tridy (objektu)

    // pokud nema metoda explicitne danou viditelnost, pouzije se public, plati zde stejna pravidla, jako u atributu, tzn. public metodu muzu volat z instance
    // pokud bych chtel pristupovat/volat, jinou metodu objektu, muzu pouzit zase $this->, napriklad $this->multiply(3);
    function add($a) {
        $this->total += $a;
    }
    
    function substract($a) {
        $this->total -= $a;
    }
    
    function divide($a) {
        $this->total /= $a;
    }
    
    function multiply($a) {
        $this->total *= $a;
    }
    
}

$calculator = new Calculator(); // vytvoreni instance tridy, tedy objektu, pomoci konstruktu new

$calculator->add(8); // volani public metody
$calculator->substract(2);
$calculator->divide(2);
$calculator->multiply(6);

print $calculator->total; // zobrazeni public atributu

print '<br />';

$calculator2 = new Calculator(); // vytvoreni dalsii instance tridy, tedy objektu, pomoci konstruktu new. Instance se, v tomto pripade, vzajemne neovlivnuji
$calculator2->add(4);
print $calculator2->total; // zobrazeni public atributu druhe instance

