<?php

// Dedeni interface, kdy BInterface dedi z AInterface a trida MyClass implementuje BInterface, to ale znamena
// ze musi definovat vsechny metody nejen z BInterface, ale i z AInterface, protoze BInterface z AInterface dedi
// Nazvy rozhrani se suffixem "Interface" viz. https://www.php-fig.org/bylaws/psr-naming-conventions/

/**
 * rozhrani A
 */
interface AInterface
{

    /**
     * Popis metody ahoj
     *
     * @param int $number1 prvni cislo
     * @param int $number2 druhe cislo
     * @return Array pole s obema cisly
     */
    public function ahoj($number1, $number2) : array; // typ navratove metody, napriklad " : array", nebo " : int"
}

/**
 * rozhrani B - dedi z rohrazni A
 */
interface BInterface extends AInterface
{

    /**
     * Popis metody zdar
     *
     * @return void
     */
    public function zdar();
}

/**
 * trida MyClass implmentuje rozhrani B (BInterface) - musi definovat vsechny metody jako z AInterface, tak i z BInterface
 */
class MyClass implements BInterface
{

    /**
     * Metoda vraci pole z poslanych cisel
     * typ navratove hodnoty, napriklad " : array" nemusi byt definovana v interface, typ se vynucuje v metode
     * ale pokud je definovan jiz v interface, musi byt i v metode a musi byt stejneho typu
     * 
     * {@inheritDoc}
     */
    public function ahoj($number1, $number2) : array // vynuceni navratoveho typu
    {
        // return 42; // Fatal error: Uncaught TypeError: Return value of MyClass::ahoj() must be of the type array, int returned
        
        return [$number1, $number2];

    }

    /**
     * Metoda vraci retezec "zdar" - neni definovan typ navratove hodnoty
     *
     * @return void
     */
    public function zdar()
    { 
        return 'zdar';
    }
}

$my = new MyClass();
print_r($my->ahoj(1, 3));
// $my->ahoj // priklad naseptani dokumentace

