<?php
// Encapsulation - zapouzdreni
// Abstraction - abstrakce, doplneni parametru pro znovupouzitelnost s konfiguraci
// more Abstraction - vetsi mira abstrakce, dekompozice

function add($a, $b) {
    return $a + $b;
}

function substract($a, $b) {
    return $a - $b;
}

function divide($a, $b) {
    return $a / $b;
}

function multiply($a, $b) {
    return $a * $b;
}

function calculation($total = 0) {
    
    $total = add($total, 8);
    
    $total = substract($total, 2);
    
    $total = divide($total, 2);
    
    $total = multiply($total, 6);
    
    return $total;
}

print calculation(); // 18
print "<br />";
print calculation(4); // 30

