<?php

// puvodni kod
/*
class Animal {
    public $legs = 0;
    public $noise = '';
    
    public function vocalize() {
        print $this->noise . " !!!";
    }

}

class Labrador extends Animal {
    public $legs = 4;
    public $noise = 'haf haf';
}

class Parrot extends Animal {
    public $legs = 1;
    public $noise = 'mnau';

    public function vocalize($words = '') {
        print "{$words} {$this->noise}";
    }
}
*/

interface Animal {
    public function getLegs();
    public function getNoise();
}

interface Dog {
    public function bark();
}

class Labrador implements Animal, Dog {
    
    public function getLegs() {
        return 4;
    }

    public function getNoise() {
        return 'haf';
    }

    public function bark() {
        for ($i = 0; $i < 3; $i++) {
            print $this->getNoise() . "!";
        }
    }

}


$lump = new Labrador();
$lump->bark();



