<?php

// Kratka ukazka vyuziti "kontroly" na interface (rozhrani), namisto tridy (class)

try {
    // ...code
    
    // intdiv(10, 0);
    // throw \Exception;

} catch (Throwable $e) {
    print $e;
    print "Bude zachycena kazda trida ktera dedi z Exception i ErrorException - protoze implementuje rozhrani Throwable";
}