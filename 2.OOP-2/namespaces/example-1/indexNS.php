<?php

// require 'MojeTridaBezNamespace.php';
require 'MojeTrida.php';

// $mojeTrida = new MojeTridaBezNamespace(); // nema definovany name, muzu ji volat primo

use \Upce\Tools\MojeTrida as MC; // misto cele cesty, lze pouzit use s aliasem
use \Upce\Tools\MojeTrida; // misto cele cesty, lze pouzit use i bez aliasu a pak se, jak alias, bere nazev tridy, tedy MojeTrida

// $mojeTrida = new MojeTrida(); // Fatal error: Uncaught Error: Class 'MojeTrida' not found in..
// $mojeTrida = new \Upce\Tools\MojeTrida(); // OK - je spravne zadany namespace
// $mojeTrida = new MC(); // OK - je pouzity alias z use
$mojeTrida = new MojeTrida(); // OK - je pouzity automaticky alias z use
$mojeTrida->mojeMetoda(); // ahoj 22 Upce\Tools\MojeTrida


// jeden z problemu namespaces jsou "stare" objekty, napriklad DateTime
$dt = new DateTime();
