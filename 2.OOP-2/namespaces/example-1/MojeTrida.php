<?php

// php 5.3
// namespace je jako absolutni cesta, nyni, nase trida MojeTrida, "zije" v Upce\Tools

namespace Upce\Tools;

use \DateTime; // Tento use zaridi, aby "stara" trida zacala fungovat

/**
 * Moje trida
 */
class MojeTrida {

    /**
     * Moje metoda
     *
     * @return void
     */
    public function mojeMetoda() {

        echo "ahoj " . __LINE__ . ' ' . __CLASS__;
        
        // PHP hleda tridy v \Upce\Tools\DateTime, protoze je zde pouzity namespace
        $dt = new DateTime() ; // Fatal error: Uncaught Error: Class 'Upce\Tools\DateTime' not found in
            
        // $dt = new \DateTime() ; // nebo pouzit lomitko zde


    }

}