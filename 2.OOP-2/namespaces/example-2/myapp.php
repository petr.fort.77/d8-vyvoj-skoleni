<?php

require_once('lib1.php');

header('Content-type: text/plain');
echo \App\Lib1\MYCONST . "\n";
echo \App\Lib1\MyFunction() . "\n";
echo \App\Lib1\MyClass::WhoAmI() . "\n";

// Vystup:
// App\Lib1\MYCONST
// App\Lib1\MyFunction
// App\Lib1\MyClass::WhoAmI