<?php
use App\Lib2;

require_once('lib1.php');
require_once('lib2.php');

header('Content-type: text/plain'); // chci mit vystup, jako text, abych mohl odradkovat pomoci "\n" a nemusel delat <br />
echo Lib2\MYCONST . "\n";
echo Lib2\MyFunction() . "\n";
echo Lib2\MyClass::WhoAmI() . "\n";

// Vystup
// App\Lib2\MYCONST
// App\Lib2\MyFunction
// App\Lib2\MyClass::WhoAmI