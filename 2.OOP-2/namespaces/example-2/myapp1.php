<?php
namespace App\Lib1; // pouziju stejne namespace, jako v lib1 a tudiz muzu pouzivat metody z namespace App\Lib1

require_once('lib1.php');
require_once('lib2.php');

header('Content-type: text/plain');
echo MYCONST . "\n";
echo MyFunction() . "\n";
echo MyClass::WhoAmI() . "\n";

// Vystup:
// App\Lib1\MYCONST
// App\Lib1\MyFunction
// App\Lib1\MyClass::WhoAmI