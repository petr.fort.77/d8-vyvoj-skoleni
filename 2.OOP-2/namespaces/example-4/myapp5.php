<?php

/*
    Autoloading s namespacem     
*/

use App\Lib1\MyClass1 as MC1;

/**
 * Vlastni autoloader
 *
 * @param string $class_name
 * @return void
 */
function mujAutoload($class_name)
{
    // print $class_name; // App\Lib1\MyClass1
        
    // prevedeni namespace do cesty
	$class = 'classes/' . str_replace('\\', '/', $class_name) . '.php'; // classes/App/Lib1/Class1/${className}.php
    include($class);
    
}

// registrace autoloadovaci funkce
spl_autoload_register('mujAutoload');

$obj1 = new MC1(); // /classes/MyClass1.php is auto-loaded

header('Content-type: text/plain');
$obj1->ahoj();
print  "\n";

