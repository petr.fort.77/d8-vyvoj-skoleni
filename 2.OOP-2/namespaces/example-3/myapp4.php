<?php

/*
    Autoloading v globalnim namespace
    spl_autoload_register 
*/

function mujAutoload($class_name)
{
    include('classes/' . $class_name . '.php');
}

// registrace autoloadovaci funkce
spl_autoload_register('mujAutoload');

header('Content-type: text/plain');
$obj1 = new MyClass1(); // /classes/MyClass1.php is auto-loaded
$obj2 = new MyClass2(); // /classes/MyClass2.php is auto-loaded

$obj1->ahoj();
print  "\n";
$obj2->ahoj();
print  "\n";

