<?php

/*
    Chyby, Vyjimky - jejich zachycovani a zpracovavani    

    Priklad znovu vyhozeni, jiz zachycene, vyjimky pro dalsi zpracovani s vnorenym blokem try
    Zachyceni vice vyjimek.
    Lze vyzkouset z retezci:
        "jmeno@upce...cz" - neni email
        "jmeno@up.cz" - neobsahuje upce
        "jmeno@upce.cz" - validni
*/

/**
 * vlastni trida vyjimky
 */
class MojeVlastniException extends Exception
{
    const E_NOT_VALID = -1;
    const E_MISSING_SUBSTR = -2;

    // vlastni metoda pro zobrazeni chyby    
    public function errorMessage()
    {
        // vlastni chybova hlaska s pouzitim standardni chybove hlasky $this->getMessage()
        if ($this->getCode() === self::E_NOT_VALID) {
            $errorMsg = ' e-mail: ' . $this->getMessage() . ' není validní';
        } else if ($this->getCode() == self::E_MISSING_SUBSTR) {
            $errorMsg = ' e-mail: ' . $this->getMessage() . ' neobsahuje řetězec upce';
        } else {
            $errorMsg = ' neznámá chyba ' . $this->getCode() . ', v e-mailu: ' . $this->getMessage();
        }
        
        return $errorMsg;
    }

}

$email = "jmeno@upce...cz";
// $email = "jmeno@up.cz";
// $email = "jmeno@upce.cz";

try {

    try {

        // kontrola na to, jestli je dany e-mail validni
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
            // vyhozeni vyjimky, pokud neni validni
            throw new MojeVlastniException($email, MojeVlastniException::E_NOT_VALID);
            // throw new MojeVlastniException($email, 42); // stejne, se vlastnim kodem (42)
        }

        // pokud mail neobsahuje "upce", vyhozim obecnou vyjimku
        if (strpos($email, "upce") === FALSE) {
            // vyhazuje obecnou vyjimku
            throw new Exception($email, MojeVlastniException::E_MISSING_SUBSTR); // vyhozeni obecne vyjimky
        }

    } catch (Exception $e) { // zpracovani obecne vyjimky
        
        // a znovu vyhozeni vlastni vyjimky
        if ($e->getCode() < 0) {
            throw new MojeVlastniException($email, $e->getCode());
        }

    }

} catch (MojeVlastniException $e) {
    echo $e->errorMessage();
}



