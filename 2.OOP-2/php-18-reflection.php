<?php

/*
Reflexe nám umožňuje zjistit, za běhu programu, informace o vybraném objektu, například jaké metody má daná třída, jaké mají parametry atd,
načíst jejich komentář, atd.

Tento 
*/


/**
 * moje trida
 */
class testClass {

    public const TEST_CONST = 'TEST public kontanty';

    private $name = 'private name';


    /**
     * Zobrazeni private atributu s poslanym retezcem
     *
     * @param string $name
     * @return void
     */
    function show(string $name){

        return $this->name . ' ' . $name;

    }

}

$obj = new ReflectionClass("testClass");
$const = $obj->getReflectionConstant("TEST_CONST");
$consts = $obj->getReflectionConstants();
$classComments = $obj->getDocComment();
$methodComment = $obj->getMethod('show')->getDocComment();

print '<pre>';
print $obj;
print '</pre>';
print '<hr />';
print '<pre>';
print $classComments;
print '</pre>';
print '<hr />';
print '<pre>';
print $methodComment;
print '</pre>';