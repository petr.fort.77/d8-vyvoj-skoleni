<?php

/*
    Chyby, Vyjimky - jejich zachycovani a zpracovavani    

    Zachyceni nezachycenych vyjimek pomoci specialniho handleru. Navrat k puvodnimu, defaultnimu, osetreni lze pomoci:
    restore_exception_handler

*/

function myExceptionHandler($exception)
{
    echo "<b>Exception:</b> " . $exception->getMessage();
}

set_exception_handler('myExceptionHandler');
// restore_exception_handler();

// nezachycena vyjimka
throw new Exception('Nezachycená výjimka');


/*
// zachycena vyjimka
try {
    throw new Exception('Nechycena vyjimka');
} catch (Exception $e) {

}
*/

