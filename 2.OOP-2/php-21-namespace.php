<?php

/*
    Namespaces (Jmenné prostory), v php od verze 5.3
    // https://symfonycasts.com/screencast/php-namespaces-in-120-seconds/namespaces

    namespaces úzce pracují standardem a PSR-4 (původně i PSR-0, ale ten je již zastaralý). 
    Tyto standardy určují, jak namespaces používat. Hlavním důvodem je zamezení konfliktů při použití knihoven 3. stran
    (například třída Database by mohl být vytvořena a používání v různých knihovnách a mohlo by dojít ke konfliktům).
    Dříve se tento problém řešit prefixem, například: ZEND_*, WP_, atd..

    Namepace musí být definován úplně nahoře PHP souboru (1. příkaz), lze pouzit i vice namespace v ramci jednoho souboru, ale
    je lepsi/standardni pouzivat pouze jeden

    pokud neni namespace definovat pouzije se globalni namespace - \ (zpětné lomítko)
    napriklad \DateTime

    namespaces mohou být hierarchické, například:
        MyProject\SubName
        MyProject\Database\MySQL
        CompanyName\MyProject\Common\Widget

    Detailní pravidla zde:
    https://www.php.net/manual/en/language.namespaces.rules.php
  
    Pokud je pro třídu (ve třídě) definován namespace je nutné k ní pak přistupovat pomocí něj
    \Upce\Tools\MojeTrida
    
    případně použít import namespace pomocí operátoru use, aby se zkrátil zapis při použití (use lze použít i s aliasem)

    namespace MojeAplikace; // Nyní soubor patří pod ns MojeAplikace

    priklad v adresari ./namespaces
    ./namespaces/index.php
    ./namespaces/MojeTrida.php
    ./namespaces/MojeTridaBezNamespace.php
*/

