<?php

/*
    Chyby, Vyjimky - jejich zachycovani a zpracovavani    
    
    NEfatalni chyby, tedy ty co nevyhazuji (Throw) vyjimku/chybu (Error) - nejsou instanci Error class
        se daji dale zpracovat pomoci set_error_handler 
    Vratit puvodni funkcnost lze pomoci restore_error_handler(void) : bool;

    Fatalni chyby, se musi zpracovat pomoci catch (Error $e) // nebo Throwable
        E_ERROR - skript nemuze pokracovat a musi byt zastaven
        E_RECOVERABLE_ERROR - 
        ...
        > Script je zastaveny dokud/pokud neni zpracovan ve vyjimce.
        
    soupis chybovych kody, je zde
    https://www.php.net/manual/en/errorfunc.constants.php

    Throwable
        Error
            ArithmeticError
                DivisionByZeroError
            AssertionError
            CompileError
                ParseError
            TypeError
                ArgumentCountError
    
*/
