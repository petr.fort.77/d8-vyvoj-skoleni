<?php

/*
    Chyby, Vyjimky - jejich zachycovani a zpracovavani    
    
    Definice vlastniho handleru pro zpracovani nezachycenych chyb. Obdobne, jako u vyjimek
    existuje set_error_handler a restore_error_handler

    priklad c.1 - vyvolani chyby E_DEPRECATED
    priklad c.2 - vyvolani chyby E_NOTICE
    priklad c.3 - vyvolani chyby E_USER_NOTICE
    priklad c.4 - fatal error
*/


/**
 * Error Handler - pro zpracovani nefatalni chyby
 *
 * @param integer $errNo [E_NOTICE - 8 |  E_DEPRECATED - 8192 | ... ]
 * @param string $errMsg
 * @param string $file
 * @param integer $line
 * @return void
 */
function myHandler(int $errNo, string $errMsg, string $file, int $line)
{
    echo "<br />Chyba číslo <strong>$errNo</strong> v souboru <strong>$file</strong> na řádku <strong>$line</strong>:<br /> <strong>$errMsg</strong>" . "<br />";
}

set_error_handler('myHandler');

// restore_error_handler(); // ukazka vraceni puvodniho/systemoveho handleru



// priklad c.1 vyvolani chyby E_DEPRECATED
class foo
{
    function bar()
    {
        echo '<i>Nejsem staticka metoda!</i><br />';
    }
}

foo::bar(); // chyba E_DEPRECATED - 8192 run-time upozorneni (notice) ze tento kod nebude fungovat v budoucich verzich php


// priklad c.2 vyvolani chyby E_NOTICE
echo $thisVariableIsNotSet; // chyba E_NOTICE - 8


// priklad c.3 vyvolani chyby pomoci trigger_error: E_USER_NOTICE - 1024
// https://www.php.net/manual/en/function.trigger-error.php
trigger_error("TEST USER NOTICE ERROR", E_USER_NOTICE);



// priklad c.4 - parse error - ten neni zachyceny
// sadas //Parse error: syntax error, unexpected end of file in - toto neni zachycene pres set_error_handler