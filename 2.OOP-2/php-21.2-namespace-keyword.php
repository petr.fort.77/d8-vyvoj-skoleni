<?php

/*
    Pokrocile pouziti namespace
    klicove slovo namespace
*/

namespace App\Lib1;

class MyClass {
    public function WhoAmI() {
        return __METHOD__;
    }
}

// pouziti konstanty __NAMESPACE__
// $c = __NAMESPACE__ . '\\MyClass';
// $m = new $c;

// pouzit klicove slova namespace
$m = new namespace\MyClass;

echo $m->WhoAmI(); // outputs: App\Lib1\MyClass::WhoAmI

// App\Lib1\MyClass::WhoAmI