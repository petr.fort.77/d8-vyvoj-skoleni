<?php

/*
Chyby, Vyjimky - jejich zachycovani a zpracovavani

Ve zpracování chyb došlo k nejvýraznějším změnám mezi verzemi 5.6 a 7.1
Aktuálně třídy Error a Exception implementují rozhraní Throwable (to bylo nově přidáno v php 7)
Zachycení lze provést až na úrovni rozhraní (interface) Throwable, které oba objekty (a všechny z nich zděděné) implementují
    
    try {
        // ...code
    } catch (Throwable $e) {
        echo "A class that inherits from either Exception or ErrorException was caught";
    }

Chyby:

Rozdil mezi chybou (Error) a vyjimkou (Exception) je v zasade ten, ze vyjimky jsou vyhazovany, 
zachycovany a nasledne zpracovany a program (vetsinou) pokracuje dal, ale u chyb dojde k zastaveni behu 
programu, toto se tyka chyb, ktere jsou vyhazovany, tzn. fatalnich chyb, ostatni - ne tak zavazne chyby -
napriklad notice, warning apod. nejsou reseny pres try catch blok, ale zpracovavaji se pres specialni
funkce/handlery.

Nezpracovani fatalnich chyb a jejich zobrazeni je take bezpecnostni riziko (mimo to, ze by chyby mely byt 
na produkci vypnute)

Zde je priklad "granularity" zachycovani vyjimek:

priklady zachyceni: 
// Zachyceni vsech vyjimek
    catch( Exception $exception ) {}
// Zachyceni vsech vyjimek vyhozenych knihovnou Upce (tedy vsechny tridy vyjimek, ktere implementuji Upce\Exception\ExceptionInterface)
    catch( Upce\Exception\ExceptionInterface $exception ) {}
// Zachyceni specificke SPL vyjimky (bez ohlednu na knihovnu)
    catch( LogicException $exception )
// Zachyceni specificke SPL vyjimky vyhozenych knihovnou Upce
    catch( BrightNucleus\Exception\LogicException $exception ) {}

SPL - The Standard PHP Library (SPL) kolekce rozhrani a trid, ktere jsou zamyslene k reseni obecnych problemu.
https://www.php.net/manual/en/intro.spl.php


ukazka rozhrani Throwable 

interface Throwable {
    // Methods 
    abstract public getMessage ( void ) : string
    abstract public getCode ( void ) : int
    abstract public getFile ( void ) : string
    abstract public getLine ( void ) : int
    abstract public getTrace ( void ) : array
    abstract public getTraceAsString ( void ) : string
    abstract public getPrevious ( void ) : Throwable
    abstract public __toString ( void ) : string
}

prehled vyjimek

// Output for 7.3.0 - 7.4.0rc1
Throwable
    Error
        ArithmeticError
            DivisionByZeroError
        AssertionError
        CompileError
            ParseError
        TypeError
            ArgumentCountError
    Exception
        ClosedGeneratorException
        DOMException
        ErrorException
        IntlException
        JsonException
        LogicException
            BadFunctionCallException
            BadMethodCallException
            DomainException
            InvalidArgumentException
            LengthException
            OutOfRangeException
        PharException
        ReflectionException
        RuntimeException
            OutOfBoundsException
            OverflowException
            PDOException
            RangeException
            UnderflowException
            UnexpectedValueException
        SodiumException

https://wiki.php.net/rfc/throwable-interface
https://www.php.net/manual/en/class.throwable.php
https://www.php.net/manual/en/class.exception.php
https://www.php.net/manual/en/class.error.php

*/