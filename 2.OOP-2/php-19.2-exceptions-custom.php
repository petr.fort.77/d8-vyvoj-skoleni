<?php

/*
    Chyby, Vyjimky - jejich zachycovani a zpracovavani    

    Priklad vytvoreni vlastni (custom) vyjimky a jeji zpracovani.
    Trida MojeVlastniException musi dedit z Exception (pripadne Error), pripadne z jejich zdedenych trid, 
        aby sla "odchytit" v catch a dale zpracovat.
    Nelze ani udelat to, aby primo implementovala Throwable (class MojeVlastniException implements Throwable)
        > "Fatal error: Class MojeVlastniException cannot implement interface Throwable, extend Exception or Error instead"
    
    Zdedena trida muze (ale nemusi) mit vlastni metodu pro zobrazeni chyby, v tomto pripade errorMessage.
*/

/**
 * Je dobre, v pojmenovani, dodrzovat urcite konvence, napriklad konci nazev "Exception"
 * V pripade anglickeho pojmenovani pak nazev nepusobi tak "hrozne"
 */
// class MyOwnException extends Exception // anglicke pojmenovani je lepsi, pokud chceme dodrzet konvence pojmenovani
// class MojeVlastniException implements Throwable // nelze - FATAL
// class MojeVlastniException // nelze - nejedna se o potomka Exception, nebo Error a proto se nezachytava
class MojeVlastniException extends Exception
{
    // vlastni metoda pro zobrazeni chyby    
    public function errorMessage()
    {
        // chybova hlaska
        $errorMsg = 'Chyba na řádku ' . $this->getLine() . ' v souboru ' . $this->getFile()
            . ': <b>' . $this->getMessage() . '</b> není validní e-mailová adresa';
        return $errorMsg;
    }

}

$email = "jmeno@upce...cz";

try {
  // kontrola na to, jestli je dany e-mail validni
  if(filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
    // vyhozeni vyjimky, pokud neni validni
    throw new MojeVlastniException($email);
    // throw new MojeVlastniException($email, 42); // stejne, se vlastnim kodem (42)
  }

// } catch (Exception $e) { // lze zpracovat o obecne pres Exception, protoze MojeVlastniException dedi z Exception
} catch (MojeVlastniException $e) { // lepe zpracovat, ale vlastni vyjimku, kdyz uz ji mame
    echo 'Výjimka, zpráva : ' . $e->errorMessage() . ", kód: " . $e->getCode(); // zobrazeni vlastni metodou
    // echo 'Vyjimka, zprava : ' . $e->getMessage() . ", kod: " . $e->getCode();
}
