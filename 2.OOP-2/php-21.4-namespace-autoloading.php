<?php

/*
Priklad autoloadingu v globalnim namespace

"/namespaces/example-3"

"/namespaces/example-3/classes/MyClass1.php"
class MyClass1 {

    public function ahoj() {
        print __CLASS__ . " ahoj";
    }

}

"/namespaces/example-3/classes/MyClass2.php"
class MyClass2 {

    public function ahoj() {
        print __CLASS__ . " ahoj";
    }

}

"/namespaces/example-3/myapp4.php"

function mujAutoload($class_name)
{
    include('classes/' . $class_name . '.php');
}
// registrace autoloadovaci funkce
spl_autoload_register('mujAutoload');

header('Content-type: text/plain');
$obj1 = new MyClass1(); // /classes/MyClass1.php is auto-loaded
$obj2 = new MyClass2(); // /classes/MyClass2.php is auto-loaded

$obj1->ahoj();
print  "\n";
$obj2->ahoj();
print  "\n";

*/