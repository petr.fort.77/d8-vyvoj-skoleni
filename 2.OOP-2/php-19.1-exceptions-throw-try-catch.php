<?php

/*
    Chyby, Vyjimky - jejich zachycovani a zpracovavani    

    Ukazka zpracovani, "rucne", vyhozene, standardni/obecne vyjimky (class Exception) s vlastni hlaskou (pripadne kodem)
    pro otestovani nezachycene vyjimky odkomentovat r.24 // checkNum(2);
    pro otestovani zachycene vyjimky (zakomentovat r.24)
        - s odkomentovanym r.17, se zachycuje obecna (Exception) vyhozena (throw) vyjimka s vlastni hlaskou
        - v pripade zakometovani r.18 a odkomentovani r.18, je vyjimka doplnena o vlastni "kod" = 10;
*/

// funkce, ktera vyhazuje (throw) vyjimku
function checkNum($number)
{
    if ($number > 1) {
        // throw new Exception("Hodnota musí byt menší, nebo rovná 0"); // defaultni kod ($code) = 0
        throw new Exception("Hodnota musí byt menší, nebo rovná 0", 10); // s kodem, napriklad 10, idelne pres konstantu
    }

}
 
// spusteni vyjimky bez osetreni
// checkNum(2); // pro test odkomentovat


try {
    checkNum(2);
    
    echo "Tato zprava se nikdy nezobrazi"; // kod kam se, pokud je vyjimka, beh programu nikdy nedostane
} catch (Exception $e) {
    echo 'Vyjimka, zprava : ' . $e->getMessage() . ", kod: " . $e->getCode();
}
